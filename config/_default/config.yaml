baseURL: https://handbook.gitlab.com
languageCode: en-US
locale: en-US
title: The GitLab Handbook

social:
  twitter: GitLab

params:
  title: The GitLab Handbook
  defaultContentLanguage: en
  locale: en-US
  description: Learn more about GitLab and what makes us tick.
  images:
      - images/gitlab-cover.png
  branch: main
  project: gitlab-com/content-sites/handbook
  plantuml:
    enable: true
    # svg: true
  mermaid:
    enable: true
  offlineSearch: true
  ui:
    sidebar_menu_compact: false
    sidebar_menu_foldable: true
    sidebar_cache_limit: 1000
    sidebar_menu_truncate: 100
    navbar_logo: true
  copyright: "GitLab"
  links:
    user:
      - name: "Contribute"
        url: "/docs/"
        icon: "fa fa-pen"
        desc: "Contribute to the internal handbook"
      - name: "GitLab Repository"
        url: "https://gitlab.com/gitlab-com/content-sites/handbook"
        icon: "fab fa-gitlab"
        desc: "Contribute on GitLab"
      - name: "Slack Discussions"
        url: "https://gitlab.slack.com/archives/C81PT2ALD"
        icon: "fab fa-slack"
        desc: "Discuss GitLab Handbook on Slack"
    handbooks:
      - name: "GitLab"
        url: "https://about.gitlab.com/"
        icon: "fab fa-gitlab"
        desc: "GitLab"
      - name: "TeamOps"
        url: "https://about.gitlab.com/teamops"
        icon: "fa-solid fa-arrows-spin"
        desc: "TeamOps"
      - name: "Handbook"
        url: "/handbook/"
        icon: "fa fa-book"
        desc: "The Handbook"
      - name: "Job Families"
        url: "/job-families/"
        icon: "fa-solid fa-users"
        desc: "Job Families"

menu:
  main:
    - weight: -2
      name: GitLab
      url: https://about.gitlab.com/
      pre: '<i class="fab fa-gitlab"></i>'
    - weight: 2
      name: TeamOps
      url: https://about.gitlab.com/teamops
      pre: '<i class="fa-solid fa-arrows-spin"></i>'

mediaTypes:
  text/netlify:
    delimiter: ""
    suffixes: [ "" ]
outputs:
  home: [ "HTML", "REDIRECTS" ]
  section: [ "HTML" ]
outputformats:
  redirects:
    baseName: "_redirects"
    isPlainText: true
    mediaType: "text/netlify"
    notAlternative: true

markup:
  goldmark:
    renderer:
      unsafe: true
  highlight:
    style: tango
    lineNos: true
    tabWidth: 2
  tableOfContents:
    startLevel: 2
    endLevel: 4
    ordered: false

security:
  funcs:
    getenv: [ "^HUGO_", "PERISCOPE_EMBED_API_KEY", "PRODUCT_ANALYTICS_APP_KEY", "PRODUCT_ANALYTICS_HOST" ]

canonifyURLs: true
enableGitInfo: true
pluralizeListTitles: false
enableEmoji: true

theme:
- gitlab.com/gitlab-com/content-sites/docsy-gitlab
- github.com/google/docsy

module:
  hugoVersion:
    extended: true
    min: 0.95
  imports:
  - path: gitlab.com/gitlab-com/content-sites/docsy-gitlab
    disable: false
  - path: github.com/google/docsy
    disable: false
  - path: github.com/google/docsy/dependencies
    disable: false
